## Durch die Augen eines Kindes - Park Simulation

Simulation eines Parks durch die Augen eines Kindes mit verschiedenen Gefahrenquellen (z. B. Wasser (See, Fluss) und Fahrradfahrer:innen) und Fokusverschiebung auf bunte, leuchtende, bodennahe Gegenstände (Insekten, Blumen, Steine, etc.).

Je nach tatsächlichem Aufwand soll es 2-3 Altersstufen zum Auswählen geben (z. B. krabbelndes Baby und laufendes Kind), interessante Gegenstände besonders hervorgehoben werden (beispielsweise eine Art glühen oder blinken, um uns Erwachsene damit stärker auf den Gegenstand zu konzentrieren und abzulenken von der Umgebung, wie es bei Kindern ist).